var Sequelize = require('sequelize'),
	express = require('express'),
	bodyParser = require('body-parser'),
	passport = require('passport'),
	bcryptjs = require('bcryptjs'),
	SteamStrategy = require('passport-steam').Strategy,
	GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
	LocalStrategy = require('passport-local').Strategy,
	BearerStrategy = require('passport-http-bearer').Strategy,
	TwitchTvStrategy = require('passport-twitchtv').Strategy
	FacebookStrategy = require('passport-facebook').Strategy;

/* Passport */
passport.serializeUser(function (user, done) {
	done(null, user);
});

passport.deserializeUser(function (obj, done) {
	done(null, obj);
});

passport.use(new SteamStrategy({
	returnURL: 'http://localhost:3000/auth/steam/callback',
	realm: 'http://localhost:3000/',
	apiKey: '42817BF9FB7277FA3D81F30D5D5B02AC'
}, function (identifier, profile, done) {
	console.log(profile);
	return done(null, profile);
}));

passport.use(new GoogleStrategy({
	clientID: '586049746488-7m5ijo0il7s2qml59g5o3k7tt277vvth.apps.googleusercontent.com',
	clientSecret: 'resal9KYILEpdpBuWp0kFK8O',
	callbackURL: 'http://localhost:3000/auth/google/callback'
}, function (accessToken, refreshToken, profile, done) {
	console.log(profile);
	return done(null, profile);
}));

passport.use(new FacebookStrategy({
	clientID: '1443706372617243',
	clientSecret: '60d01429609209e9547678eb6257d097',
	callbackURL: 'http://localhost:3000/auth/facebook/callback'
}, function (accessToken, refreshToken, profile, done) {
	console.log(profile);
	return done(null, profile);
}));

passport.use(new TwitchTvStrategy({
	clientID: 'l5qwyqgnd7isf27ll0sexd2emkpqw62',
	clientSecret: 'jcm8moxwo1pt1mqk226g42wru3ltuva',
	callbackURL: 'http://localhost:3000/auth/twitchtv/callback',
	scope: 'user_read'
}, function (accessToken, refreshToken, profile, done) {
	console.log(profile);
	return done(null, profile);
}));

passport.use(new LocalStrategy(function (username, password, done) {
	User.findOne({
		where: {
			username: username
		}
	}).then(function (user) {
		if (user) {
			if (user.validPassword(password)) {
				done(null, user);
			} else {
				done(null, false, {message: 'Incorrect password.'});
			}
		} else {
			done(null, false, {message: 'Incorrect username.'});
		}
	}).catch(function (err) {
		done(err);
	});
}));

passport.use(new BearerStrategy({}, function (token, done) {
	User.findOne({
		where: {
			username: token
		}
	}).then(function (user) {
		if (user) {
			done(null, user);
		} else {
			done(null, false, {message: 'Incorrect username.'});
		}
	}).catch(function (err) {
		done(err);
	});
}));

/* Sequelize configuration */
var db = new Sequelize('database.db', 'username', 'password', {
		host: 'localhost',
		dialect: 'sqlite',
		pool: {
			max: 5,
			min: 0,
			idle: 100
		},
		storage: './data/database.db'
	});

var User = db.define('User', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true
	},
	email: {
		type: Sequelize.STRING(254),
		allowNull: false,
		unique: true
	},
	name: {
		type: Sequelize.STRING(150),
		allowNull: false
	},
	username: {
		type: Sequelize.STRING(50),
		allowNull: false,
		unique: true
	},
	password: {
		type: Sequelize.STRING(255),
		allowNull: false,
		set: function (valor) {
			this.setDataValue('password', this.encryptPassword(valor));
		}
	},
	salt: {
		type: Sequelize.STRING(255),
		allowNull: false
	},
	steamID: {
		type: Sequelize.STRING(150),
		allowNull: true,
		unique: true
	},
	googleID: {
		type: Sequelize.STRING(150),
		allowNull: true,
		unique: true
	},
	facebookID: {
		type: Sequelize.STRING(150),
		allowNull: true,
		unique: true
	},
	twitchtvID: {
		type: Sequelize.STRING(150),
		allowNull: true,
		unique: true
	}
}, {
	underscored: true,
	freezeTableName: true,
	tableName: 'users',
	instanceMethods: {
		validPassword: function (password) {
			return bcryptjs.compareSync(password, this.password);
		},
		encryptPassword: function (password) {
			return bcryptjs.hashSync(password, this.salt);
		}
	}
});
User.beforeValidate(function (user) {
	if (user.options.isNewRecord) {
		user.salt = bcryptjs.genSaltSync(10);
	}
});

// Drop and re-create all tables
db.sync({force: true}).then(function() {
	User.create({email: 'oliverkra@gmail.com', name: 'Oliver Kraemer', username: 'oliver', password: 'oliver'})
		.then(function(user) {
			console.log("Usuário " + user.name + " incluido com sucesso!");
		});
});

/* Express */
var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Passport
app.use(passport.initialize());
app.use(passport.session());

var routerAuth = express.Router();

routerAuth.get(
	'/steam',
	passport.authenticate('steam', { failureRedirect: '/auth/login' }),
	function (req, res) {
		res.redirect('/api/users');
	}
);

routerAuth.get(
	'/steam/callback',
	passport.authenticate('steam', { failureRedirect: '/auth/login' }),
	function (req, res) {
		res.redirect('/api/users');
	}
);

routerAuth.get(
	'/google',
	passport.authenticate('google',
		{
			scope: ['https://www.googleapis.com/auth/plus.login'],
			failureRedirect: '/auth/login'
		}),
	function (req, res) {
		res.redirect('/api/users');
	}
);

routerAuth.get(
	'/google/callback',
	passport.authenticate('google', { failureRedirect: '/auth/login' }),
	function (req, res) {
		res.redirect('/api/users');
	}
);

routerAuth.get('/facebook', passport.authenticate('facebook'));
routerAuth.get(
	'/facebook/callback',
	passport.authenticate('facebook', { failureRedirect: '/auth/login' }),
	function (req, res) {
		res.redirect('/api/users');
	}
);

app.get('/auth/twitchtv', passport.authenticate('twitchtv'));

app.get('/auth/twitchtv/callback', 
	passport.authenticate('twitchtv', { failureRedirect: '/auth/login' }),
	function (req, res) {
		res.redirect('/api/users');
	}
);

routerAuth.get(
	'/login',
	function (req, res) {
		res.end(
			'<html><body><form action="/auth/login" method="post">' +
			'<div><label>Username:</label><input type="text" name="username"/></div>' +
			'<div><label>Password:</label><input type="password" name="password"/></div>' +
			'<div><input type="submit" value="Log In"/></div>' +
			'</form></body></html>'
		);
	}
);

routerAuth.post(
	'/login',
	passport.authenticate('local', { failureRedirect: '/auth/login' }),
	function (req, res) {
		res.redirect('/api/users');
	}
);

routerAuth.get('/logout', function(req, res){
  req.logout();
  res.json({message: 'Successfully logout'});
});


// API

var router = express.Router();
var userAction = router.route('/users');

userAction.get(function (req, res) {
	User.findAndCountAll({
		where: {},
		limit: 100,
		offset: 0
	}).then(function (result) {
		res.json(result);
	}).catch(function (err) {
		res.status(500)
			.json({message: err.message});
	});
});

userAction.post(function (req, res){
	console.log(req.body);
	User.create({
		name: req.body.name,
		email: req.body.email
	})
	.then(function(user){
		res.json({id: user.id});
	})
	.catch(function(err){
		res.status(500)
			.json({message: err.message});
	});
});

userAction = router.route('/users/:id');

userAction.get(function (req, res){
	User.findById(req.params.id)
		.then(function (user) {
			if (user) {
				res.json(user);
			} else {
				res.status(404).json({});
			}
		}).catch(function (err) {
			res.status(500)
				.json({message: err.message})
		});
});

userAction.put(function (req, res){
	User.findById(req.params.id)
		.then(function (user) {
			if (!user) {
				res.status(404).json({});
				return
			}

			user.update({
				name: req.body.name
			}).then(function (user) {
				res.json(user)
			}).catch(function (err) {
				res.status(500)
					.json({message: err.message})
			});
		}).catch(function (err) {
			res.status(500)
				.json({message: err.message})
		});
});

userAction.delete(function (req, res){
	User.findById(req.params.id)
		.then(function (user) {
			if (!user) {
				res.status(404).json({});
				return
			}

			user.destroy()
				.then(function () {
					res.json({message: 'User successfully deleted'})
				}).catch(function (err) {
					res.status(500)
						.json({message: err.message})
				})
		}).catch(function (err) {
			res.status(500)
				.json({message: err.message})
		});
});

app.use('/api', passport.authenticate('bearer', { session: false }), router);
app.use('/auth', routerAuth);

app.listen(3000, function() {
	console.log('Express server running on port %d', 3000);
});

function ensureAuthenticated(req, res, next) {
	return passport.authenticate('bearer', { session: false });
	// var bearer = passport.authenticate('bearer', { session: false });
	// bearer(req, res, function (req, res) {
	// 	if (req.isAuthenticated()) {
	// 		console.log('nanana aqui!!!!');
	// 		next();
	// 	} else {
	// 		console.log('aqui!!!!');
	// 		res.status(401).json({message: 'Authentication required.'});
	// 	}
	// });
}